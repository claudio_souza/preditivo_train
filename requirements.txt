# Web servers.
Flask~=1.1.2
gunicorn~=20.1.0

# Data analysis and ML.
pandas~=1.2.4
scikit-learn~=0.24.2
python-dateutil~=2.8.1

# Data reporting.
seaborn~=0.11.1
matplotlib~=3.2.0

# Storage.
tables~=3.6.1
joblib~=1.0.1

# GCP utils.
tqdm~=4.60.0
pyarrow~=4.0.0

# GCP.
google-cloud-storage~=1.38.0
google-cloud-bigquery~=2.13.1
google-cloud-bigquery-storage~=2.4.0