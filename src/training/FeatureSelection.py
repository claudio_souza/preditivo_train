from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import SelectFromModel


def create_feature_selector(X, y):
    feature_selector = SelectFromModel(
        estimator=ExtraTreesClassifier(
            n_estimators=30,
            random_state=42,
            n_jobs=-1
        )
    ).fit(X, y)

    return feature_selector


def get_selected_features(X, feature_selector):
    selected_features_indices = feature_selector.get_support()
    selected_features = X.columns[selected_features_indices]
    return selected_features


class FeatureSelection:
    def __init__(self):
        self.selected_features = None

    def run(self, X, y):
        feature_selector = create_feature_selector(X, y)
        self.selected_features = get_selected_features(X, feature_selector)

        return self.selected_features
